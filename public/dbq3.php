<?php

	/*
		versioning:
			1.	??????? ??????
			2.	????????? ??????? ???????? ??????, ????? ??? ?? ??????
			3.	export / import possibility, set names option
	*/

/*

queries returning values

+ delete - boolean
+ create - boolean
+ select - resource
- insert - boolean (mysql_num_fields(): supplied argument is not a valid MySQL result resource)
+ update - boolean
+ alter - boolean
+ drop - boolean

*/

	if(strstr($_SERVER["HTTP_HOST"], 'test.')) {
		define("TESTMODE", TRUE);
	}
	else {
		define("TESTMODE", FALSE);
	}

	define("DBE_LOGIN", "dbq");
	define("DBE_PASSW", "letmein");


	define("MEMO_LIMIT", 0);
	define("SESS_PROLONG", 1000 * 60 * 10);

	define("ROW_LIMIT", 0);
	
	define("DEF_HOST", "db");
	if(TESTMODE) {
		define("DEF_USER", "project_test");
		define("DEF_PASS", "");
		define("DEF_BASE", "project_test");
	}
	else {
		define("DEF_USER", "root");
		define("DEF_PASS", "root");
		define("DEF_BASE", "db");
	}
	define("DEF_CHAR", "utf8");
	


	function left($str, $sub) { return substr($str, 0, strlen($sub)) == $sub; }


	function set_style() {

?>

<html>
<head>
<STYLE type="text/css">
	body, table { font: 11px Verdana; }
	input, select { font: 11px Verdana; width: 100px; }
	textarea { font: 12px Courier New; }
	.text { font: 11px Verdana; }
	.clickable { border-bottom: 1px dotted; cursor: pointer; cursor: hand; }
</STYLE>
<meta http-equiv="Content-Type" content="text/html; charset=<?= CHAR ?>">
</head>

<body>
<?php

	}


#	mysqldump --add-drop-table -hlocalhost -ujurfirma -p jurfirma site_firm > 1
	function backup_table($tbl, $filter = array()) {

		$data = array();
		$fields = array();

		$qr = 
			"SELECT * FROM $tbl ";

		if((int)ROW_LIMIT) {
			$qr .= "LIMIT 0, " . (int)ROW_LIMIT;
		}

		$q = mysql_query($qr);

		for($i = 0; $i < mysql_num_fields($q); $i++) $fields[] = mysql_field_name($q, $i);

		$i = 0;

		while($res = mysql_fetch_object($q)) {

			$data[$i] = array();

			foreach($fields as $fld) {

				$data[$i][$fld] = $res->$fld;

			}

			$i++;

		}

		unset($res, $q);

		$qr = "";

		if($filter["_struc"]) {
			$qr .= "/*
	TABLE STRUC $tbl
*/

";
			$q = mysql_query("SHOW CREATE TABLE $tbl ");
			$res = mysql_fetch_array($q);
			$qr .= str_replace("\n", "\r\n", $res["Create Table"]) . ";

";
		}

		$qr .= "
/*
	TABLE DATA $tbl
*/

";

		$qr_sql = "";
		foreach($data as $row) {
			if($qr_sql) $qr_sql .= ",\r\n";
			$qr_row = "";
			foreach($row as $fld => $fdata) {
				if($qr_row) $qr_row .= ",";
				$qr_row .= "'" . mysql_real_escape_string($fdata) . "'";
			}
			if($qr_row) $qr_sql .= "(" . $qr_row . ")";
		}

		unset($data);

		if($qr_sql) $qr .= "INSERT INTO $tbl VALUES \r\n" . $qr_sql . ";


";

		unset($qr_sql);

		return $qr;

	}


	function prn($var, $name = "") {
		if(!$name) $name = "var";
		switch(gettype($var)) {
			case "object" :
				$type = "print_r";
				break;
			case "array" :
				$type = "print_r";
				break;
			default :
				$type = "echo";
				break;
		}
		if(($type == 'echo') && !$var) {
			echo " <b>($name is empty)</b> ";
			return;
		}
		switch($type) {
			case "print_r" :
				echo "<pre style='color: #000000; font: 12px Courier New'>$name:\r";
				print_r($var);
				echo "</pre>";
				break;
			case "echo" :
				echo " <b style='color: #000000; font: 12px Courier New'>($name = $var)</b> <br>";
				break;
		}
	}


	function quoteHTML($v) {
		if(is_array($v)) $v = "";
		return htmlspecialchars($v, ENT_QUOTES);
	}

	
	function list_tables() {
		$tbls = array();
		if($q = mysql_query("show tables")) {
			while($res = mysql_fetch_array($q)) {
				$tbls[] = $res[0];
			}
		}
		return $tbls;
	}

	function query_form($qr) {

		set_style();

		if($_POST["showsql"]) {
			$chk = "checked";
		}
		else {
			$chk = "";
		}

		if($_POST["showsql"]) {
			$qr = "";
		}

		if(!$qr) $qr = "SELECT * FROM";

		$tbls = list_tables();

?>

<script type="text/javascript">
	var prolongtime = parseInt(<?= SESS_PROLONG ?>);
	function getHTTPObject() {
		if (typeof XMLHttpRequest != 'undefined') {
			return new XMLHttpRequest();
		}
		try {
			return new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				return new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {
				// we're so sorry.. =(
			}
		}
		return false;
	}
	var http = getHTTPObject();
	var http_working = false;
	function prolongsession() {
		//if(!working) {
			http.abort();
			http.open(
				"GET",
				"<?= $_SERVER["REQUEST_URI"] ?>?prolong=1",
				true
			);
			var post = "";
			http.send(null);
		//}
		setTimeout(prolongsession, prolongtime);
	}
	setTimeout(prolongsession, prolongtime);
</script>

<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=01 style="width: 640px; ">

<FORM NAME='operate' ACTION='' METHOD='POST'>
<INPUT TYPE='hidden' NAME='operate' VALUE='1'>

<TR>
	<TD valign=top><TEXTAREA NAME='qr' id="qr" style="height: 200px; width: 100%; "><?= $qr ?></TEXTAREA></TD>
</TR>
<TR>
	<TD>
		<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0 style="width: 100%; ">
			<TR>
				<TD nowrap="true" style="width: 50%; "><INPUT TYPE='submit' VALUE='Execute'>&nbsp;Don't show SQL:&nbsp;<INPUT TYPE='checkbox' NAME='showsql' VALUE=1 <?= $chk ?> STYLE='width: 20px;'>&nbsp;|&nbsp;&nbsp;<SELECT CLASS='text' name='table' onchange='if(this.value) document.operate.qr.value += " " + this.value + " "; this.value = ""; '>
				<OPTION VALUE=''>.</OPTION><?php

	foreach($tbls as $n) {

		echo "<OPTION VALUE='" . addslashes($n) . "'>$n</OPTION>
";

	}

		 ?></SELECT></TD>
			</TR>
		</TABLE>
	</TD>
</TR>

</FORM>
<TR>
	<TD><table cellspacing="0" cellpadding="0" width="100%">
		<tr>
			<td>
				<FORM NAME='enter_params' ACTION='' METHOD='POST'>
				<INPUT TYPE='hidden' NAME='op' VALUE='enter_params'>
				<INPUT TYPE='hidden' NAME='php_438_bug' VALUE='1'>
				<INPUT TYPE='submit' VALUE='Config'>
				</FORM>
			</td>
			<td style="text-align: right; ">
				<FORM NAME='f_options' ACTION='' METHOD='POST' enctype="multipart/form-data">
				<INPUT TYPE='hidden' NAME='op' VALUE='options'>
				<div id="options">
					<span onclick="document.getElementById('options').style.display = 'none'; document.getElementById('export').style.display = 'block'; " class="clickable">Export SQL</span>
					| <span onclick="document.getElementById('options').style.display = 'none'; document.getElementById('import').style.display = 'block'; " class="clickable">Import SQL</span>
				</div>

				<div id="import" style="display: none; ">
					Upload File:&nbsp;<input style="width: 183px; " type="file" name="sqlfile">&nbsp;<input type="submit" name="import" value="Import" />
				</div>
				<div id="export" style="display: none; "><select name="exporttbl">
				<option value="">(whole db)</option>
<?php
				foreach($tbls as $n) {

		echo "<OPTION VALUE='" . addslashes($n) . "'>$n</OPTION>
";

	}
?>
				</select>&nbsp;<input type="submit" type="submit" name="export" value="Export" /></div>
				</FORM>
			</td>
		</tr>
	</table></TD>
</TR>

</TABLE>

<?php

	}


	function params_form($host, $user, $pass, $base, $char) {

		set_style();

		if(!$host) $host = DEF_HOST;
		if(!$user) $user = DEF_USER;
		if(!$pass) $pass = DEF_PASS;
		if(!$base) $base = DEF_BASE;
		if(!$char) $char = DEF_CHAR;

?>

<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=04>

<FORM NAME='set_params' ACTION='' METHOD='POST'>
<INPUT TYPE='hidden' NAME='op' VALUE='set_params'>

<TR>
	<TD valign=top>host:</TD>
	<TD><INPUT TYPE='text' NAME='mysql_host' value='<?= $host ?>'></TD>
</TR>
<TR>
	<TD valign=top>user:</TD>
	<TD><INPUT TYPE='text' NAME='mysql_user' value='<?= $user ?>'></TD>
</TR>
<TR>
	<TD valign=top>pass:</TD>
	<TD><INPUT TYPE='password' NAME='mysql_pass' value='<?= $pass ?>'></TD>
</TR>
<TR>
	<TD valign=top>base:</TD>
	<TD>
	<!-- <INPUT TYPE='text' NAME='mysql_base' value='<?= $base ?>'> -->
	<SELECT NAME='mysql_base'>
<?php
	$q = mysql_query("SHOW DATABASES;");
	while($res = mysql_fetch_object($q)) {
		$b = $res->Database;
		if($b == $base) $sel = " selected"; else $sel = "";
		echo "<OPTION$sel value='" . quoteHTML($b) . "'>$b</OPTION>
";
	}
?>
	</SELECT></TD>
</TR>
<TR>
	<TD valign=top>charset:</TD>
	<TD><INPUT TYPE='text' NAME='mysql_char' value='<?= $char ?>'></TD>
</TR>
<TR>
	<TD>&nbsp;</TD>
	<TD><INPUT TYPE='submit' VALUE='Save'></TD>
</TR>

</FORM>

</TABLE>

<?php

	}

	function display_result($q) {

		if($q) {

			switch($type = gettype($q)) {
				case "resource" :
					$n = (int)mysql_num_rows($q);
					break;
				default :
					$n = (int)mysql_affected_rows();
					break;
			}

			$result = "Ok";

		}
		else {

			$result = "FALSE";

		}

		echo "
Result: <B>$result</B>. Rows: <B>$n</B>.<br><br>
";

		if($n && $type == 'resource') {

			echo "
<table border=01 cellspacing=0 cellpadding=01>
	<tr>
";

			for($i = 0; $i < mysql_num_fields($q); $i++) {

				echo "<th>".(mysql_field_name($q,$i))."</th>";

			}

			echo "</tr>
";

			while($res=mysql_fetch_array($q)) {

				$row = "";

				for($j = 0;$j < mysql_num_fields($q); $j++)

					$row .= "<td>" . (MEMO_LIMIT ? substr($res[$j], 0, MEMO_LIMIT) : $res[$j]) . "</td>
";

				echo "<tr>$row</tr>
";

			}

			echo "</table><br>
";

		}

	}

	function db_connect($host, $user, $pass, $base, $char) {

		if(!$r1 = mysql_connect($host, $user, $pass)) {
			echo "<p>Error: cannot connect to mysql server.</p>";
		}
		if(!$r2 = mysql_select_db($base)) {
			echo "<p>Error: cannot select database.</p>";
		}

		if($r1 && r2 && $char) {
			mysql_query("SET NAMES " . $char);
		}

		return ($r1 && $r2);

	}


	function split_sql(&$ret, $sql, $release)
	{
		$release = "32332";

		$sql          = trim($sql);
		$sql_len      = strlen($sql);
		$char         = '';
		$string_start = '';
		$in_string    = FALSE;
		$time0        = time();

		for ($i = 0; $i < $sql_len; ++$i) {
			$char = $sql[$i];

			// We are in a string, check for not escaped end of strings except for
			// backquotes that can't be escaped
			if ($in_string) {
				for (;;) {
					$i         = strpos($sql, $string_start, $i);
					// No end of string found -> add the current substring to the
					// returned array
					if (!$i) {
						$ret[] = $sql;
						return TRUE;
					}
					// Backquotes or no backslashes before quotes: it's indeed the
					// end of the string -> exit the loop
					else if ($string_start == '`' || $sql[$i-1] != '\\') {
						$string_start      = '';
						$in_string         = FALSE;
						break;
					}
					// one or more Backslashes before the presumed end of string...
					else {
						// ... first checks for escaped backslashes
						$j                     = 2;
						$escaped_backslash     = FALSE;
						while ($i-$j > 0 && $sql[$i-$j] == '\\') {
							$escaped_backslash = !$escaped_backslash;
							$j++;
						}
						// ... if escaped backslashes: it's really the end of the
						// string -> exit the loop
						if ($escaped_backslash) {
							$string_start  = '';
							$in_string     = FALSE;
							break;
						}
						// ... else loop
						else {
							$i++;
						}
					} // end if...elseif...else
				} // end for
			} // end if (in string)

			// We are not in a string, first check for delimiter...
			else if ($char == ';') {
				// if delimiter found, add the parsed part to the returned array
				$ret[]      = substr($sql, 0, $i);
				$sql        = ltrim(substr($sql, min($i + 1, $sql_len)));
				$sql_len    = strlen($sql);
				if ($sql_len) {
					$i      = -1;
				} else {
					// The submited statement(s) end(s) here
					return TRUE;
				}
			} // end else if (is delimiter)

			// ... then check for start of a string,...
			else if (($char == '"') || ($char == '\'') || ($char == '`')) {
				$in_string    = TRUE;
				$string_start = $char;
			} // end else if (is start of string)

			// ... for start of a comment (and remove this comment if found)...
			else if ($char == '#'
					 || ($char == ' ' && $i > 1 && $sql[$i-2] . $sql[$i-1] == '--')) {
				// starting position of the comment depends on the comment type
				$start_of_comment = (($sql[$i] == '#') ? $i : $i-2);
				// if no "\n" exits in the remaining string, checks for "\r"
				// (Mac eol style)
				$end_of_comment   = (strpos(' ' . $sql, "\012", $i+2))
								  ? strpos(' ' . $sql, "\012", $i+2)
								  : strpos(' ' . $sql, "\015", $i+2);
				if (!$end_of_comment) {
					// no eol found after '#', add the parsed part to the returned
					// array if required and exit
					if ($start_of_comment > 0) {
						$ret[]    = trim(substr($sql, 0, $start_of_comment));
					}
					return TRUE;
				} else {
					$sql          = substr($sql, 0, $start_of_comment)
								  . ltrim(substr($sql, $end_of_comment));
					$sql_len      = strlen($sql);
					$i--;
				} // end if...else
			} // end else if (is comment)

			// ... and finally disactivate the "/*!...*/" syntax if MySQL < 3.22.07
			else if ($release < 32270
					 && ($char == '!' && $i > 1  && $sql[$i-2] . $sql[$i-1] == '/*')) {
				$sql[$i] = ' ';
			} // end else if

			// loic1: send a fake header each 30 sec. to bypass browser timeout
			$time1     = time();
			if ($time1 >= $time0 + 30) {
				$time0 = $time1;
				# [!] header('X-pmaPing: Pong');
			} // end if
		} // end for

		// add any rest to the returned array
		if (!empty($sql) && preg_match('@[^[:space:]]+@', $sql)) {
			$ret[] = $sql;
		}

		return TRUE;
	}


	#
	# =========================================================================


	session_start();

	if($_GET["prolong"] == 1) die;

	if($_POST["login"]) {

		$login = $_POST["user_login"];
		$passw = $_POST["user_passw"];

		if(($login == DBE_LOGIN) && ($passw == DBE_PASSW)) {

			srand((double)microtime()*1000000);

			$uid = md5($login . $passw);

			$_SESSION["uid"] = $uid;
			$_SESSION["mysql_host"] = DEF_HOST;
			$_SESSION["mysql_user"] = DEF_USER;
			$_SESSION["mysql_pass"] = DEF_PASS;
			$_SESSION["mysql_base"] = DEF_BASE;
			$_SESSION["mysql_char"] = DEF_CHAR;

			header("Location: " . $_SERVER["REQUEST_URI"]);
			die;

		}

	}
	else {

		$allow_access = (md5(DBE_LOGIN . DBE_PASSW) == $_SESSION["uid"]);

	}

	if($allow_access) {

		if($_POST["op"] == 'set_params') {

			$mysql_host = $_POST["mysql_host"];
			$mysql_user = $_POST["mysql_user"];
			$mysql_pass = $_POST["mysql_pass"];
			$mysql_base = $_POST["mysql_base"];
			$mysql_char = $_POST["mysql_char"];

			$_SESSION["mysql_host"] = $mysql_host;
			$_SESSION["mysql_user"] = $mysql_user;
			$_SESSION["mysql_pass"] = $mysql_pass;
			$_SESSION["mysql_base"] = $mysql_base;
			$_SESSION["mysql_char"] = $mysql_char;

			set_style();

			echo "<p>MYSQL parameters saved.</p>";

		}

		$host = $_SESSION["mysql_host"];
		$user = $_SESSION["mysql_user"];
		$pass = $_SESSION["mysql_pass"];
		$base = $_SESSION["mysql_base"];
		$char = $_SESSION["mysql_char"];

		define("CHAR", $char);

		if($host && $user && $base) {
			$link = db_connect($host, $user, $pass, $base, $char);
		}

		if($_POST["op"] == 'enter_params') {

			params_form($host, $user, $pass, $base, $char);
			exit;

		}

		if($_POST["op"] == "options") {

			// import operation
			if(isset($_FILES["sqlfile"])) {
				$fl = $_FILES["sqlfile"];
				switch((int)$fl["error"]) {
					case 4:
						// do nothing
						break;
					case 0:
						// read sql from file
						if($fl["size"] > 10) {
							$upload_ok = 1;
							$_POST["qr"] = implode("", file($fl["tmp_name"]));
							if(get_magic_quotes_gpc()) $_POST["qr"] = addslashes($_POST["qr"]);
							unlink($fl["tmp_name"]);
						}
						else {
							echo "<p>Uploaded file is empty or too small.</p>";
						}
						break;
					case 1:
						echo "<p>File size exceeds upload_max_filesize (" . ini_get("upload_max_filesize") . ").</p>";
						break;
					default:
						// error happened
						echo "<p>Error while uploading SQL-file. Error code is " . (int)$fl["error"] . ".</p>";
						break;
				}
			}

			// export operation

			if($_POST["export"]) {
				$tbl = $_POST["exporttbl"];
				if(!$tbl) {
					$tbl = list_tables();
					$filename = $base . "_" . date("Y-m-d");
				}
				else {
					$filename = $tbl . "_" . date("Y-m-d");
					$tbl = array($tbl);
				}
				$filename .= ".sql";
				if(is_array($tbl)) {
					$exportsql = "";
					foreach($tbl as $table) {

						if(
							1
							//!left($table, "phpmv")
						) {
							/*$q = mysql_query("SELECT COUNT(*) cnt FROM `$table` ");
							$res = mysql_fetch_object($q);
							if($res->cnt < 10000)*/
								$exportsql .= backup_table($table, array(
									"_struc" => 1,
								));
						}

					}
					//
					header("Content-Type: application/force-download");
					header("Content-Type: application/octet-stream");
					header("Content-Type: application/download");
					header("Expires: Mon, 26 May 1997 05:00:00 GMT");
					header("Pragma: no-cache");
					header("Content-Length: " . strlen($exportsql));
					header('Content-Disposition: attachment; filename="' . $filename . '"');
					echo $exportsql;
					die;
				}
			}

		}

		if($qr = $_POST["qr"]) {

			set_style();

			if(get_magic_quotes_gpc()) $qr = stripslashes($qr);

			$qar = array();

			$release = "32332";

			// get mysql version
			$q = mysql_query("SELECT VERSION() AS ver");
			$ver = mysql_fetch_object($q);
			$ver = substr($ver->ver, 0, 1);

			split_sql($qar, $qr, $release);

#echo "<pre>";
#print_r($qar);
#echo "</pre>";

			foreach($qar as $query) {

				$query = trim($query);

				if($ver == 3) {
					if(strtoupper(substr($query, 0, 7)) == "CREATE ") {
						$query = ereg_replace("ENGINE\=[^\;]+", "TYPE=MyISAM;", $query);
					}
				}

				if($q = mysql_query($query)) {

					display_result($q);

				}
				else {

					echo "<p>Error In Query: <br><pre>$query</pre><br><b>" . mysql_error() . "</b></p>";


				}

			}

			if($upload_ok) {
				unset($_POST["qr"]);
				unset($qr);
			}

		}

		query_form($qr);

	}
	else {


		set_style();

?>

<FORM NAME='login' ACTION='' METHOD='POST'>
<INPUT TYPE='hidden' NAME='login' VALUE='1'>
<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=03>
<TR>
	<TD>Login:</TD>
	<TD><INPUT NAME='user_login'></TD>
</TR>
<TR>
	<TD>Password:</TD>
	<TD><INPUT NAME='user_passw' TYPE='password'></TD>
</TR>
	<TD COLSPAN=2 ALIGN=right><INPUT TYPE='submit' VALUE='Go'></TD>
</TABLE>
</FORM>

<?php

	}

?>

</body>
</html>
